
## [1.0.0] 12-02-2024

First release

## [1.0.1] 10-07-2024

Updated rxdart dependency
Renamed library file: streamer.dart to flutter_streamer.dart

## [1.1.0] 25-09-2024

Added the StateStreamer class that add some superpowers to the existing Streamer class
It makes state management more easier
Check out the new example's logic class

## [2.0.0] 20-10-2024

> Breaking changes !
> Some existing methods have new parameters
> And brand new methods have been added

Check out when, where, filter and select methods on the Git repo.

## [2.0.1] 05-11-2024

Bring back rxdart: 0.27.7 because of a SwitchMap regression in 0.28.0 :/
https://github.com/ReactiveX/rxdart/issues/764

## [2.0.2] 06-11-2024

Small code clean up

## [2.1.0] 20-10-2024

> Breaking changes !
> Some existing methods have new parameters
> And brand new methods have been added

Check out given, parse, filter and select methods on the Git repo.

With these new parameters you can now easily apply filters to trigger events:
- only when [when]'s value is true (if [when] provided)
- only when [distinctBy]'s value changed (if [distinctBy] provided)
- mapped to the second given type [V]

## [2.1.1] 15-01-2025

Streamer's getLastBefore method now use List.lastIndexOf instead of List.indexOf
to handle cases where history contains two identical events

## [2.2.0] 16-01-2025

Created AutoDisposeSubscriptions mixin which is used by the streamer
and which can be used wherever you use streams !

It just add a List<StreamSubscription> subscriptions property 
and automatically close them in its dispose method :)