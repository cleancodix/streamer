# streamer

A pure dart state management class based on standard Dart's Stream.

## Keep it simple 

And implements your own pattern, 

```
    class CounterController extends Streamer<int> {
      CounterController(super.initialValue);
      
      void increment(Increment event) => emit(++last);
    }
```


## A simple logic class that extends Streamer

```
class CounterLogic extends Streamer<CounterState> {
  int _value;

  CounterLogic(this._value) : super(CounterState(_value)) {
    on<Increment>(onIncrement);
  }

  set value(int newValue) {
    if (newValue >= 10) {
      emit(MaxReached());
    } else {
      _value = newValue;
    }
  }

  int get value => _value;

  void onIncrement(Increment event) => emit(CounterState(++value));
}
```


### Check the example section for a simple implementation of the EASE pattern

- The EASE pattern is inspired from the BloC pattern
  - In the BloC pattern, UI emits Events and a Cubit/Bloc class responds with States
  - Which could be a bit tricky to handle View's logic (navigation, showing a dialog)
- The EASE pattern defines Events, Actions, States & Errors classes
  - They're used to handle communication between our Logic class and our View through a stream
  - Because a simple view should just depends on a stream of Events/States/Errors and emits user Actions
  - And a simple logic should just depends on a stream of user Actions and emits Events/States/Errors
- This way it's pretty easy from the View to trigger navigation or dialogs
- You View does not even need to know Logic implementation
- You Logic does not even need to know View implementation