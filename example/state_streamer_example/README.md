# stream_listener_widget_example

The classic counter example to show how it's easy to use the streamer package.

Here we implemented the EASE pattern* with the streamer package.

*We defined Events, Actions, States & Errors classes to handle communication between our Logic class and our View through a stream.

Note: we used flutter_inject to inject our Logic class and stream_listener_widget to handle view's logic (showing dialogs).