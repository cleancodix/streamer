import 'package:flutter_streamer/flutter_streamer.dart';

import 'counter_io.dart';

class CounterLogic extends StateStreamer<CounterState, CounterIO> {
  CounterLogic(int value) : super(CounterState(value)) {
    on<Increment>(onIncrement);
    on<Decrement>(onDecrement);
  }

  @override
  CounterIO? mapState(CounterState newState) {
    if (newState.value > 10) {
      return MaxReached();
    } else if (newState.value < 0) {
      return UnauthorizedValue('A counter under zero is not allowed');
    }
    return newState;
  }

  void onIncrement(Increment event) => state = CounterState(state.value + 1);
  void onDecrement(Decrement event) => state = CounterState(state.value - 1);
}
