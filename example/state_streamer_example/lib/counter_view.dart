import 'package:flutter/material.dart';
import 'package:flutter_inject/flutter_inject.dart';
import 'package:flutter_streamer/flutter_streamer.dart';
import 'package:stream_listener_widget/stream_listener_widget.dart';

import 'counter_io.dart';
import 'counter_logic.dart';

class CounterView extends StatelessWidget {
  final int initialValue;

  const CounterView({super.key, required this.initialValue});

  void _showDialog(BuildContext context, String message) => showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Text(message),
            ),
          );
        },
      );

  void _onMaxReached(BuildContext context) => _showDialog(context, 'Your counter has reached max value');

  void _onError(BuildContext context, CounterError event) => _showDialog(context, event.message);

  @override
  Widget build(BuildContext context) {
    // Here we use flutter_inject to simply inject view's dependencies (our logic class here)
    // Note that flutter_inject will automatically dispose injected dependencies :D
    return Inject<Streamer<CounterIO>>(
        factory: (context) => CounterLogic(initialValue),
        builder: (context) {
          final streamer = Dependency.get<Streamer<CounterIO>>(context);
          // Here we use stream_listener_widget package to listen stream without rebuilding child widget
          return StreamListener(
            listeners: [
              (context) => streamer.on<MaxReached>((e) => _onMaxReached(context)),
              (context) => streamer.on<CounterError>((e) => _onError(context, e)),
            ],
            child: Scaffold(
              appBar: AppBar(title: const Text('Counter demo')),
              body: Center(
                child: StreamBuilder<CounterState>(
                  stream: streamer.only<CounterState>(),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) return Text(snapshot.error.toString());
                    if (!snapshot.hasData) return const CircularProgressIndicator();
                    final state = snapshot.data!;
                    return Text('Counter: ${state.value}');
                  },
                ),
              ),
              floatingActionButton: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FloatingActionButton(
                    onPressed: () => streamer.emit(Increment()),
                    child: const Icon(Icons.add),
                  ),
                  FloatingActionButton(
                    onPressed: () => streamer.emit(Decrement()),
                    child: const Icon(Icons.remove),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
