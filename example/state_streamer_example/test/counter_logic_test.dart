import 'package:flutter_test/flutter_test.dart';
import 'package:streamer_example/counter_io.dart';
import 'package:streamer_example/counter_logic.dart';

void main() {
  late int initialValue;
  late CounterLogic logic;

  setUp(() {
    initialValue = 5;
    logic = CounterLogic(initialValue);
  });

  group('CounterLogic test', () {
    test('Increment test', () async {
      // Given an initial counter value
      expect(logic.state.value, initialValue);
      // When
      logic.emit(Increment());
      await pumpEventQueue();
      // Then
      expect(logic.state.value, initialValue + 1);
      expect(logic.history.any((e) => e is MaxReached), false);
      // When
      logic.emit(Increment());
      logic.emit(Increment());
      logic.emit(Increment());
      logic.emit(Increment());
      await pumpEventQueue();
      // Then
      expect(logic.state.value, initialValue + 5);
      expect(logic.history.any((e) => e is MaxReached), true);
    });
    test('Decrement test', () async {
      // Given an initial counter value
      expect(logic.state.value, initialValue);
      // When
      logic.emit(Decrement());
      await pumpEventQueue();
      // Then
      expect(logic.state.value, initialValue - 1);
      expect(logic.history.any((e) => e is UnauthorizedValue), false);
      // When
      logic.emit(Decrement());
      logic.emit(Decrement());
      logic.emit(Decrement());
      logic.emit(Decrement());
      logic.emit(Decrement());
      await pumpEventQueue();
      // Then
      expect(logic.state.value, initialValue - 6);
      expect(logic.history.any((e) => e is UnauthorizedValue), true);
    });
  });
}
