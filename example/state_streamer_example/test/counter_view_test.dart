import 'package:flutter/material.dart';
import 'package:flutter_inject/flutter_inject.dart';
import 'package:flutter_streamer/flutter_streamer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:streamer_example/counter_io.dart';
import 'package:streamer_example/counter_view.dart';

void main() {
  late CounterState initialState;
  late Streamer<CounterIO> streamer;

  setUp(() {
    initialState = CounterState(5);
    streamer = Streamer<CounterIO>(initialState);
  });

  group('CounterView test', () {
    testWidgets('Streamer test', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(
        MaterialApp(
          home: Inject(
              override: true,
              factory: (context) => streamer,
              builder: (context) {
                return CounterView(initialValue: initialState.value);
              }),
        ),
      );

      // We trigger another frame because StreamBuilder is in "Waiting" state
      await tester.pump();

      // Verify that our counter starts at given value
      expect(streamer.last, initialState);
      expect(find.text('Counter: ${initialState.value}'), findsOneWidget);

      // Tap the '+' icon and trigger a frame
      await tester.tap(find.byIcon(Icons.add));

      // Verify that our logic streamed the right event
      expect(streamer.last.runtimeType, Increment);

      // Tap the '-' icon and trigger a frame
      await tester.tap(find.byIcon(Icons.remove));

      // Verify that our logic streamed the right event
      expect(streamer.last.runtimeType, Decrement);
    });

    testWidgets('Original Flutter counter test', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(MaterialApp(home: CounterView(initialValue: initialState.value)));

      // We trigger another frame because StreamBuilder is in "Waiting" state
      await tester.pump();

      // Verify that our counter starts at 0
      expect(find.text('Counter: ${initialState.value}'), findsOneWidget);
      expect(find.text('Counter: ${initialState.value + 1}'), findsNothing);

      // Tap the '+' icon and trigger a frame
      await tester.tap(find.byIcon(Icons.add));
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter: ${initialState.value}'), findsNothing);
      expect(find.text('Counter: ${initialState.value + 1}'), findsOneWidget);
    });

    testWidgets('Show dialog when max reached', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(MaterialApp(home: CounterView(initialValue: initialState.value)));

      // We trigger another frame because StreamBuilder is in "Waiting" state
      await tester.pump();

      // Verify that our counter starts at given value
      expect(find.text('Counter: ${initialState.value}'), findsOneWidget);

      // Tap the '+' icon 5 times and trigger a frame
      const buttonTaps = 5;
      for (var i = 0; i < buttonTaps; i++) {
        await tester.tap(find.byIcon(Icons.add));
      }
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter:${initialState.value}'), findsNothing);
      expect(find.text('Counter: ${initialState.value + buttonTaps}'), findsOneWidget);

      // Verify that max dialog is displayed
      final dialog = find.text('Your counter has reached max value');
      expect(dialog, findsOneWidget);

      // Tap outside of the dialog and trigger a frame
      await tester.tap(find.text('Counter demo'), warnIfMissed: false);
      // Note: warnIfMissed is set to false to avoid a warning
      // because the text "Counter demo" is behind the Dialog's barrier and so it's not directly tapped
      await tester.pump();

      // Verify that max dialog is displayed
      expect(dialog, findsNothing);
    });

    testWidgets('Show dialog when counter under zero', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(MaterialApp(home: CounterView(initialValue: initialState.value)));

      // We trigger another frame because StreamBuilder is in "Waiting" state
      await tester.pump();

      // Verify that our counter starts at given value
      expect(find.text('Counter: ${initialState.value}'), findsOneWidget);

      // Tap the '-' icon 6 times and trigger a frame
      const buttonTaps = 6;
      for (var i = 0; i < buttonTaps; i++) {
        await tester.tap(find.byIcon(Icons.remove));
      }
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter:${initialState.value}'), findsNothing);
      expect(find.text('Counter: ${initialState.value - buttonTaps}'), findsOneWidget);

      // Verify that max dialog is displayed
      final dialog = find.text('A counter under zero is not allowed');
      expect(dialog, findsOneWidget);

      // Tap outside of the dialog and trigger a frame
      await tester.tap(find.text('Counter demo'), warnIfMissed: false);
      // Note: warnIfMissed is set to false to avoid a warning
      // because the text "Counter demo" is behind the Dialog's barrier and so it's not directly tapped
      await tester.pump();

      // Verify that max dialog is displayed
      expect(dialog, findsNothing);
    });
  });
}
