sealed class CounterIO {}

/// Here is an example of a simple implementation of the EASE pattern
///
/// Events, States and Errors are always emitted from the Logic class
/// Actions are always emitted from the View
///
/// Note: to easily test Events equality we could use the freezed package
///

/// Events
sealed class CounterEvent extends CounterIO {}

class MaxReached extends CounterEvent {}

/// Actions
sealed class CounterAction extends CounterIO {}

class Increment extends CounterAction {}

class Decrement extends CounterAction {}

/// States
class CounterState extends CounterIO {
  final int value;
  CounterState(this.value);
}

class Loading extends CounterState {
  Loading(super.value);
}

/// Errors
sealed class CounterError extends CounterIO {
  final String message;

  CounterError(this.message);
}

class UnauthorizedValue extends CounterError {
  UnauthorizedValue(super.message);
}

class UnknownError extends CounterError {
  UnknownError(super.message);
}
