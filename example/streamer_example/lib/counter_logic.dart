import 'package:flutter_streamer/flutter_streamer.dart';

import 'counter_io.dart';

class CounterLogic extends Streamer<CounterIO> {
  int _value;

  CounterLogic(this._value) : super(CounterState(_value)) {
    on<Increment>(onIncrement);
    on<Decrement>(onDecrement);
  }

  set value(int newValue) {
    if (newValue >= 10) {
      emit(MaxReached());
    } else if (newValue <= 0) {
      emit(UnauthorizedValue('A counter under zero is not allowed'));
    } else {
      _value = newValue;
    }
  }

  int get value => _value;

  void onIncrement(Increment event) => emit(CounterState(++value));
  void onDecrement(Decrement event) => emit(CounterState(--value));
}
