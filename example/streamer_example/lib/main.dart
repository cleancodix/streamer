import 'package:flutter/material.dart';

import 'counter_view.dart';

void main() => runApp(
      const MaterialApp(
        home: CounterView(initialValue: 5),
      ),
    );
