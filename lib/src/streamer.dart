library streamer;

import 'dart:async';
import 'dart:developer';

import 'package:rxdart/rxdart.dart';

///
/// A simple mixin to auto dispose streams' subscriptions
/// Use it wherever you use streams
///
/// ```dart
/// class CounterLogic with AutoDisposeSubscriptions {
///
///   CounterLogic(Stream<int> valueStream) {
///     subscriptions.add(valueStream.listen(defaultListener));
///   }
///
///   void onValue(int value) => print('New value: $value');
/// }
/// ```
///
mixin AutoDisposeSubscriptions {
  /// The stream subscriptions that gonna be disposed
  final subscriptions = <StreamSubscription>[];

  /// The dispose method to cancel all our subscriptions
  void dispose() {
    for (final subscription in subscriptions) {
      subscription.cancel();
    }
  }
}

///
/// The class you should inherits to implement simple state management based on standard Dart's Stream.
/// ```dart
/// class CounterLogic extends Streamer<CounterIO> {
///   int _value;
///
///   CounterLogic(this._value) : super(CounterState(_value)) {
///     on<Increment>(onIncrement);
///     on<Decrement>(onDecrement);
///   }
///
///   set value(int newValue) {
///     if (newValue >= 10) {
///     emit(MaxReached());
///     } else if (newValue <= 0) {
///     emit(UnauthorizedValue('A counter under zero is not allowed'));
///     } else {
///     _value = newValue;
///     }
///   }
///
///   int get value => _value;
///
///   void onIncrement(Increment event) => emit(CounterState(++value));
///   void onDecrement(Decrement event) => emit(CounterState(--value));
/// }
/// ```
///
class Streamer<T extends Object> with AutoDisposeSubscriptions {
  /// The stream controller behind all this logic
  late final _controller = BehaviorSubject<T>(
    onListen: () => log('$loggerPrefix Listen<$T>()'),
    onCancel: () => log('$loggerPrefix Cancel<$T>()'),
  );

  /// Our controller's stream
  late final stream = _controller.stream;

  /// All the events streamed since the beginning
  final history = <T>[];

  /// The constructor of our [Streamer] class
  /// Note that you could provide an initial value
  Streamer([T? initialValue]) {
    subscriptions.add(stream.listen(defaultListener));
    if (initialValue != null) _controller.add(initialValue);
  }

  /// The default stream listener which will add any event to the [history] array
  void defaultListener(T value) {
    history.add(value);
  }

  /// A simple getter to access easily to the last emitted event
  T? get last => history.lastOrNull;

  /// A method getter to access the last emitted event of the given type
  U? getLast<U extends T>() => history.whereType<U>().lastOrNull;

  /// A method getter to access the last emitted event of the given type and before the given event
  U? getLastBefore<U extends T>(T event) {
    U? result;
    final eventIndex = history.lastIndexOf(event);
    if (eventIndex > 0) {
      final historyBeforeEvent = history.getRange(0, eventIndex);
      result = historyBeforeEvent.whereType<U>().lastOrNull;
    }
    return result;
  }

  /// The method that you should call whenever you need to broadcast and event
  void emit(T event) {
    if (!_controller.isClosed) {
      log('$loggerPrefix Emit($event)');
      _controller.add(event);
    } else {
      log('$loggerPrefix Stream closed, can\'t emit $event');
    }
  }

  /// A stream filtered by the given type [U]
  /// and triggered only when [when]'s value is true (if [when] provided)
  /// (note: that when's method is called with previous [U?] and current [U] events)
  /// and triggered only when [distinctBy]'s value changed (if [distinctBy] provided)
  Stream<U> filter<U extends T>({
    dynamic Function(U)? distinctBy,
    bool Function(U?, U)? when,
  }) {
    var result = stream.where((e) => e is U).cast<U>();
    if (when != null) {
      result = result.mapNotNull((e) {
        final previous = getLastBefore<U>(e);
        if (when.call(previous, e)) return e;
        return null;
      });
    }
    if (distinctBy != null) {
      result = result.mapNotNull((e) {
        final previous = getLastBefore<U>(e);
        if (previous == null) return e;
        if (previous == e || distinctBy(previous) == distinctBy(e)) return null;
        return e;
      });
    }
    return result;
  }

  /// A stream filtered by the given type [U]
  Stream<U> only<U extends T>() {
    return filter<U>();
  }

  /// A stream filtered by the given type [U]
  /// and triggered only when [distinctBy]'s value changed (if [distinctBy] provided)
  Stream<U> select<U extends T>(dynamic Function(U) distinctBy) {
    return filter<U>(distinctBy: distinctBy);
  }

  /// A stream filtered by the given type [U]
  /// and triggered only when [when]'s value is true (if [when] provided)
  /// (note: that when's method is called with previous [U?] and current [U] events)
  /// and triggered only when [distinctBy]'s value changed (if [distinctBy] provided)
  /// and mapped to the second given type [V]
  Stream<V> map<U extends T, V>(
    V Function(U) mapper, {
    bool Function(U?, U)? when,
    dynamic Function(U)? distinctBy,
  }) {
    return filter<U>(distinctBy: distinctBy, when: when).map(mapper);
  }

  /// A method to start listening a stream filtered by type [U]
  /// and which will execute [then] method for each event
  /// and which will return a [StreamSubscription] of type [U]
  StreamSubscription<U> on<U extends T>(void Function(U) then) {
    final subscription = filter<U>().listen(then);
    log('$loggerPrefix On<$U>()');
    subscriptions.add(subscription);
    return subscription;
  }

  /// A method to start listening a stream filtered by type [U]
  /// and triggered only when [when]'s value is true (if [when] provided)
  /// (note: that when's method is called with previous [U?] and current [U] events)
  /// and triggered only when [distinctBy]'s value changed (if [distinctBy] provided)
  /// and which will execute [then] method for each event
  /// and which will return a [StreamSubscription] of type [U]
  StreamSubscription<U> given<U extends T>({
    bool Function(U?, U)? when,
    required void Function(U) then,
    dynamic Function(U)? distinctBy,
  }) {
    final subscription = filter<U>(distinctBy: distinctBy, when: when).listen(then);
    log('$loggerPrefix Given<$U>()');
    subscriptions.add(subscription);
    return subscription;
  }

  /// A method to start listening a stream filtered by type [U]
  /// and triggered only when [when]'s value is true (if [when] provided)
  /// (note: that when's method is called with previous [U?] and current [U] events)
  /// and triggered only when [distinctBy]'s value changed (if [distinctBy] provided)
  /// and mapped to the second given type [V]
  /// and which will execute [then] method for each event
  /// and which will return a [StreamSubscription] of type [U]
  StreamSubscription<V> parse<U extends T, V>({
    required V Function(U) mapper,
    bool Function(U?, U)? when,
    required void Function(V) then,
    dynamic Function(U)? distinctBy,
  }) {
    final subscription = map<U, V>(mapper, distinctBy: distinctBy, when: when).listen(then);
    log('$loggerPrefix Parse<$U, $V>()');
    subscriptions.add(subscription);
    return subscription;
  }

  @override

  /// The dispose method to cancel all our subscriptions and to close our stream controller
  void dispose() {
    log('$loggerPrefix Dispose()');
    super.dispose();
    _controller.close();
  }

  /// A getter that defines logs prefix
  String get loggerPrefix => '[$runtimeType:$hashCode]';
}

///
/// The class you should inherits to implement simple state management based on standard Dart's Stream.
/// ```dart
/// class CounterLogic extends StateStreamer<CounterIO, CounterState> {
///
///   CounterLogic(int value) : super(CounterState(value)) {
///     on<Increment>(onIncrement);
///     on<Decrement>(onDecrement);
///   }
///
///   @override
///   CounterIO? mapState(CounterState newState) {
///     if (newState.value > 10) {
///       return MaxReached();
///     } else if (newState.value < 0) {
///       return UnauthorizedValue('A counter under zero is not allowed');
///     }
///     return newState;
///   }
///
///   void onIncrement(Increment event) => state = CounterState(state.value + 1);
///   void onDecrement(Decrement event) => state = CounterState(state.value - 1);
/// }
/// ```
///
class StateStreamer<U extends T, T extends Object> extends Streamer<T> {
  /// All the state events streamed since the beginning
  final states = <U>[];

  /// The constructor of our [StateStreamer] class
  /// Note the optional initial value must be a state
  StateStreamer([U? super.initialValue]);

  /// The last emitted state event
  U? _state;

  /// A simple getter to access easily to the last emitted state event
  U get state => _state ?? states.last;

  // A simple setter to trigger easily a new state event
  set state(U newState) {
    onState(newState);
    final mappedState = mapState(newState);
    if (mappedState != null) {
      if (mappedState is U) _state = mappedState;
      super.emit(mappedState);
    }
  }

  // A method to override to trigger an action on any state event
  void onState(U newState) {}

  // A method to override to filter any state event
  T? mapState(U newState) => newState;

  /// An override of the default stream listener in order to add state events to the [states] array
  @override
  void defaultListener(T value) {
    if (value is U) states.add(value);
    super.defaultListener(value);
  }
}
